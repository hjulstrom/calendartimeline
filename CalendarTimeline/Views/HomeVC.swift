//
//  HomeVC.swift
//  CalendarTimeline
//
//  Created by Niclas Hjulström on 2019-07-04.
//  Copyright © 2019 Niclas Hjulström. All rights reserved.
//

import EventKit
import SwiftDate
import TimelineTableViewCell
import UIKit

class HomeVC: UITableViewController {
    
    private let eventStore = EKEventStore()
    private var calendars: [EKCalendar]? = nil
    private var timeline: Timeline? = nil
    
    override func viewDidLoad() {
        // Register TimelineTableViewCell Nib
        let bundle = Bundle(for: TimelineTableViewCell.self)
        let nibUrl = bundle.url(forResource: "TimelineTableViewCell", withExtension: "bundle")
        let timelineTableViewCellNib = UINib(nibName: "TimelineTableViewCell",
                                             bundle: Bundle(url: nibUrl!)!)
        tableView.register(timelineTableViewCellNib, forCellReuseIdentifier: "TimelineTableViewCell")
        
        // Add notification and load Calendars
        NotificationCenter.default.addObserver(forName: .calendarAccessAllowed, object: nil, queue: OperationQueue.main) { (Notification) in
            self.calendarsLoaded()
        }
        loadCalendars()
        self.title = "Calendar Timeline"
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        // Remove separationlines from table
        tableView.separatorStyle = UITableViewCell.SeparatorStyle.none
    }

    // Calendar functions
    public func loadCalendars() {
        switch(EKEventStore.authorizationStatus(for: .event)){
        case EKAuthorizationStatus.notDetermined:
            // This happens on the first-run
            self.calRequestAccess()
        case EKAuthorizationStatus.authorized:
            self.calAccessAllowed()
        case EKAuthorizationStatus.denied, EKAuthorizationStatus.restricted:
            self.calAccessDenied()
        @unknown default:
            fatalError()
        }
    }
    
    private func calRequestAccess(){
        eventStore.requestAccess(to: .event, completion: {
            (accessGranted: Bool, error: Error?) in
            DispatchQueue.main.async(execute: {
                (accessGranted) ? self.calAccessAllowed() : self.calAccessDenied()
            })
        })
    }
    
    private func calAccessAllowed() {
        // When Auth-check for calendars has run and access is allowed this function runs
        // Load calendars to class variable
        self.calendars = eventStore.calendars(for: .event)
        // Notify that the calendars is loaded
        NotificationCenter.default.post(name: .calendarAccessAllowed, object: nil)
    }
    
    private func calAccessDenied() {
        // When Auth-check for calendars has run and access is denied this function runs
        // Notify that calendar access is denied.
        NotificationCenter.default.post(name: .calendarAccessDenied, object: nil)
    }
    
    func calendarsLoaded(){
        // Here goes code that will be run when we recieve calendarAccessAllowed notification
        let starting = Date().in(region: .current)
        let ending = Date().in(region: .current) + 7.days
        // Get Timeline with data from TimelineDataSource
        self.timeline = TimelineDataSource().calendarEvents(from: starting, to: ending, calendars: calendars!)
    }
    
    // TableView functions
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return timeline?.count ?? 0
        
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return timeline?.section(section).count ?? 0
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return timeline?.section(section).title ?? ""
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath)-> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TimelineTableViewCell", for: indexPath) as! TimelineTableViewCell
        
        let timelineItem = timeline!.item(section: indexPath.section, item: indexPath.row)
        
        // Timeline
        cell.timelinePoint               =  timelineItem.timelinePoint
        cell.timeline.frontColor         =  timelineItem.timelineFrontColor
        cell.timeline.backColor          =  timelineItem.timelineBackColor
        cell.lineInfoLabel.text          =  timelineItem.lineInfo
        
        // Bubble
        cell.bubbleColor                 =  timelineItem.bubbleColor
        
        // Title
        cell.titleLabel.text             =  timelineItem.title
        cell.titleLabel.font             = UIFont.preferredFont(forTextStyle: .headline)
        
        // Description
        cell.descriptionLabel.text       =  timelineItem.description
        cell.descriptionLabel.textColor  =  UIColor.black
        cell.descriptionLabel.font = UIFont.preferredFont(forTextStyle: .headline)
        
        // Images
        cell.thumbnailImageView.image    =  timelineItem.tumbnail ?? nil
        cell.illustrationImageView.image =  timelineItem.illustration ?? nil
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
     // Here goes code for what happens when a Timeline Item is selected
    }

}

extension Notification.Name {
    static let calendarAccessAllowed = Notification.Name("calendarAccessAllowed")
    static let calendarAccessDenied = Notification.Name("calendarAccessDenied")
}
