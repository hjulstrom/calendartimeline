//
//  TimelineDataSource.swift
//  CalendarTimeline
//
//  Created by Niclas Hjulström on 2019-07-04.
//  Copyright © 2019 Niclas Hjulström. All rights reserved.
//

import EventKit
import SwiftDate
import UIKit

class TimelineDataSource{
    
    public var timelineFrontColor: UIColor = UIColor.lightGray
    public var timelineBackColor: UIColor = UIColor.lightGray
    
    public func calendarEvents(from: DateInRegion, to: DateInRegion, calendars: [EKCalendar])->Timeline {
        let eventStore = EKEventStore()
        let timeline = Timeline()
        
        //Make array with enumerated dates with one day increments.
        let dates = DateInRegion.enumerateDates(from: from.dateAtStartOf(.day), to: to.dateAtEndOf(.day), increment: 1.days)
        
        // Loop through each day to make new Timeline Section
        dates.forEach { (date) in
            // Make a new section with section title like this example "Thursday (4 July 2019)"
            let section = TimelineSection(title: date.toFormat("EEEE (d MMMM yyyy)", locale: Locale.current))
            
            // Load events from calendars with matching dates
            let predicate = eventStore.predicateForEvents(withStart: date.date, end: date.dateAtEndOf(.day).date, calendars: calendars)
            let events = eventStore.events(matching:predicate)
            print(date)
            // Loop through each event to make new Timeline Item
            events.forEach({ (event) in
                // Make a String that contains start- and endtime of the event
                let calEventStart = event.startDate.in(region: .current)
                let calEventEnd = event.endDate.in(region: .current)
                let eventTime:String =  calEventStart.toFormat("HH:mm", locale: Locale.current) + " - " + calEventEnd.toFormat("HH:mm", locale: Locale.current)
                
                // Make a new Timeline Item
                let item = TimelineItem(
                    timelinePoint:      nil,    // nil value will make default TimelinePoint
                    // If Timeline Item is the first or last the color will be clear. Else we use class variables
                    timelineFrontColor: (event == events.first) ? UIColor.clear : self.timelineFrontColor,
                    timelineBackColor:  (event == events.last) ? UIColor.clear : self.timelineBackColor,
                    bubbleColor:        UIColor(cgColor: event.calendar.cgColor),
                    title:              event.title,
                    description:        eventTime,
                    lineInfo:           nil,    // Not implemented yet
                    tumbnail:           nil,    // Not implemented yet
                    illustration:       nil     // Not implemented yet
                )
                section.add(item) // Add Timeline Item to Timeline Section
            })
            timeline.add(section) // Add Timeline Section to Timeline
        }
        return timeline
    }
    
    
    
    
}

