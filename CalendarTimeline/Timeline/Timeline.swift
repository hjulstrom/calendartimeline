//
//  Timeline.swift
//  CalendarTimeline
//
//  Created by Niclas Hjulström on 2019-07-04.
//  Copyright © 2019 Niclas Hjulström. All rights reserved.
//

import TimelineTableViewCell

class Timeline{
    public var count: Int { get {return self.timelineSections.count} }
    private var timelineSections: [TimelineSection] = []
    
    public func add(_ section: TimelineSection){
        self.timelineSections.append(section)
    }
    
    public func section(_ index: Int)->TimelineSection {
        return self.timelineSections[index]
    }
    
    public func item(section sectionId: Int, item itemId: Int)->TimelineItem {
        return self.section(sectionId).item(itemId)
    }
}

class TimelineSection {
    public var count: Int { get {return self.timelineItems.count} }
    private var timelineItems: [TimelineItem] = []
    public var title: String
    
    init(title: String) {
        self.title = title
    }
    
    public func add(_ item: TimelineItem){
        self.timelineItems.append(item)
    }
    
    public func item(_ item: Int)->TimelineItem {
        return self.timelineItems[item]
    }
}

class TimelineItem {
    var timelinePoint: TimelinePoint
    var timelineFrontColor: UIColor
    var timelineBackColor: UIColor
    var bubbleColor: UIColor
    var title: String
    var description: String
    var lineInfo: String?
    var tumbnail: UIImage?
    var illustration: UIImage?
    
    init(timelinePoint: TimelinePoint?, timelineFrontColor:UIColor, timelineBackColor: UIColor, bubbleColor: UIColor, title:String, description:String, lineInfo:String?, tumbnail: UIImage?, illustration: UIImage?) {
        self.timelinePoint = timelinePoint ?? TimelinePoint()
        self.timelineFrontColor = timelineFrontColor
        self.timelineBackColor = timelineBackColor
        self.bubbleColor = bubbleColor
        self.title = title
        self.description = description
        self.lineInfo = lineInfo
        self.tumbnail = tumbnail
        self.illustration = illustration
    }
}
