# CalendarTimeline

CalendarTimeline is an iOS application made in Swift that shows a view of calendar events in a timeline style. It is based on CocoaPods projects TimelineTableViewCell [Link](https://cocoapods.org/pods/TimelineTableViewCell) and uses SwiftDate [Link](https://cocoapods.org/pods/SwiftDate) to manage dates.

I created this project to learn Swift and iOS development. This project will be extended with new functionality and major changes can be made along the way of learning.

You are welcome to use this project under the MIT license and I would be very happy for all feedback.

Documentation and screenshots will be available in Docs folder. 

## Installation

This project uses CocoaPods [Link](https://cocoapods.org). To install you need CocoaPods installed and then run following command in terminal in the project directory:

```bash
pod install
```

## Usage
Open the project with the .xcworkspace in Xcode.


## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

## License
[MIT](https://choosealicense.com/licenses/mit/)