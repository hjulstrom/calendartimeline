# CalendarTimeline Documentation

## General Info
CalendarTimeline is a project made in Swift for iOS devices. It is built on the CocoaPods project "TimelineTableViewCell" [Link](https://cocoapods.org/pods/TimelineTableViewCell) and extends with Calendar support (EventKit) and new data handling.

## Filestructure
* CalendarTimeline/Timeline/Timeline.swift - Contains classes for Timeline structure
* CalendarTimeline/Timeline/TimelineDataSource.swift - Contains class for Timeline DataSource. Getting the data from different sources to make a Timeline object.
* CalendarTimeline/Views/HomeVC.swift - This is the Main ViewController.
* CalendarTimeline/AppDelegate.swift
* CalendarTimeline/Assets.xcassets
* CalendarTimeline/Info.plist
* Base.lproj/LaunchScreen.storyboard
* Base.lproj/Main.storyboard

* Docs - This folder contains this documentation file and screenshots of the Views.

* Podfile - contains information about CocoaPods required for this project.
* LICENSE - License information

## Timeline Class description
Today it exists 4 classes related to Timeline. It is divided for easier handling of TableViews.
* Timeline - This class contains objects of Timeline Section and Timeline Items.
* TimelineSection - This class contains Timeline Item objects and also defines the title of section.
* TimelineItem - This class contains the row information for TableViewCell.
* TimelineDataSource - This class contains functions to be able to create Timeline objects from different datasources.

For example TimelineItem contains information about events like title, starttime, endtime and description of the event. TimelineSection contains objects of each TimelineItem and also contains a title with the date the events has. The base Timeline object contains Timeline Section objects and also functions to get access to both Sections and Items.

TimelineDataSource class contains a function to collect data, create the different objects and return a Timeline object that can be used in a TableView.

## ViewController description
The application is currently built on one ViewController (HomeVC) and also has a Navigation Controller (currently only configured in Storyboard).
### HomeVC
HomeVC is a UITableViewController. It is the Root View Controller that Navigation Controller will show. It uses a ViewCell template from a NIB file in the CocoaPods project TimeTableViewCell.

The class handles TableView functions and also it handles Calendar service functions.